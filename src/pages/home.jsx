import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button
} from 'framework7-react';

const HomePage = () => (
  <Page name="home">
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      
      <NavTitle sliding>EnsaQuizz</NavTitle>
      
      <NavTitleLarge>EnsaQuizz</NavTitleLarge>
    </Navbar>

    {/* Page content */}
    <Block strong>
      <p>Bienvenue dans notre quizz</p>

      <div class="block">
          <div className='question-section'>
						<div className='question-count'>
							<span>Question 1</span>/4
						</div>
						<div className='question-text'>Texte pour la question</div>
					</div>
					<div className='answer-section'>
						<button>Q 1</button>
						<button>Q 2</button>
						<button>Q 3</button>
						<button>Q 4</button>
					</div>
      </div>

    </Block>
    

    
  </Page>
);
export default HomePage;